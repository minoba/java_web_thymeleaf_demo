package nl.bioinf.wis_on_thymeleaf.servlets;

import nl.bioinf.wis_on_thymeleaf.config.WebConfig;
import nl.bioinf.wis_on_thymeleaf.model.Movie;
import nl.bioinf.wis_on_thymeleaf.model.Person;
import nl.bioinf.wis_on_thymeleaf.model.Role;
import nl.bioinf.wis_on_thymeleaf.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@WebServlet(name = "ThymeleafDemoServlet", urlPatterns = "/demo.thyme")
public class ThymeleafDemoServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String phraseType = request.getParameter("phrase_category");
        HttpSession session = request.getSession();
        session.setAttribute("logged_in_user", new User("Henk", "henk@example.com", Role.USER));

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("warning", "you must be over 18 to drive a car");
        ctx.setVariable("my_date", new Date());
        ctx.setVariable("my_number", 31415.9265359);
        ctx.setVariable("movies", Movie.getAllMovies());
        ctx.setVariable("movies_year_sorter", (Comparator<Movie>) (o1, o2) -> Integer.compare(o1.getYear(), o2.getYear()));
        ctx.setVariable("movies_year_sorter", new Comparator<Movie>() {
            @Override
            public int compare(Movie m1, Movie m2) {
                return Integer.compare(m1.getYear(), m2.getYear());
            }
        });
        ctx.setVariable("users", User.getSome());
        ctx.setVariable("name_of_bird_selector", "fav_birds");
        ctx.setVariable("bird_groups", List.of("raptors", "songbirds", "waders", "wildfowl"));
        ctx.setVariable("the_pub", "The Bear Inn");


        ctx.setVariable("person", new Person("Judy", "Steinberg"));

        templateEngine.process("thymeleaf_demo", ctx, response.getWriter());
    }
}
