package nl.bioinf.wis_on_thymeleaf.servlets;

import nl.bioinf.wis_on_thymeleaf.config.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "PushDemoServlet", urlPatterns = "/chatserver", asyncSupported=true)
public class PushDemoServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private List<AsyncContext> contexts = new LinkedList<>();

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<AsyncContext> asyncContexts = new ArrayList<>(this.contexts);
        this.contexts.clear();

        String name = request.getParameter("name");
        String message = request.getParameter("message");
        String chatMessage = "<p><b>" + name + "</b><br/>" + message + "</p>";
        ServletContext servletContext = request.getServletContext();
        if (servletContext.getAttribute("chat_messages") == null) {
            servletContext.setAttribute("chat_messages", chatMessage);
        } else {
            String currentMessages = (String) servletContext.getAttribute("chat_messages");
            servletContext.setAttribute("messages", chatMessage + currentMessages);
        }

        for (AsyncContext asyncContext : asyncContexts) {
            try (PrintWriter writer = asyncContext.getResponse().getWriter()) {
                writer.println(chatMessage);
                writer.flush();
                asyncContext.complete();
            } catch (Exception ex) {
            }
        }

        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);

        WebContext ctx = new WebContext(
                request,
                response,
                servletContext,
                request.getLocale());
        ctx.setVariable("currentDate", new Date());
        templateEngine.process("push_demo", ctx, response.getWriter());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final AsyncContext asyncContext = request.startAsync(request, response);
        asyncContext.setTimeout(10 * 60 * 1000);
        contexts.add(asyncContext);
    }
}
