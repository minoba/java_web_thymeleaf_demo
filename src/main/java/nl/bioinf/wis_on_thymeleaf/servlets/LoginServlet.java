package nl.bioinf.wis_on_thymeleaf.servlets;

import nl.bioinf.wis_on_thymeleaf.config.WebConfig;
import nl.bioinf.wis_on_thymeleaf.model.Role;
import nl.bioinf.wis_on_thymeleaf.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Creation date: Dec 10, 2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        String nextPage;


        if (session.isNew() || session.getAttribute("user") == null) {
            boolean authenticated = authenticate(username, password);
            if (authenticated) {
                session.setAttribute("user", new User("Henk", "henk@example.com", Role.USER));
                nextPage = "mainSite";
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "mainSite";
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

    private boolean authenticate(String username, String password) {
        return username.equals("Henk") && password.equals("henk");
    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "Fill out the login form");
        ctx.setVariable("message_type", "info");
        templateEngine.process("login", ctx, response.getWriter());
    }
}
